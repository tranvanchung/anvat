package zooog.jp.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.mysql.cj.util.StringUtils;

import zooog.jp.common.LoginAccess;
import zooog.jp.model.Login;

/**
 * Servlet implementation class LoginController
 */
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginController() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");

		PrintWriter out = response.getWriter();
		LoginAccess loginAccess = new LoginAccess();
		Gson gson = new Gson();
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String ajax = request.getParameter("ajax");

		String logout = request.getParameter("logout");
		HttpSession session = request.getSession();

		if (!StringUtils.isNullOrEmpty(ajax) && !StringUtils.isNullOrEmpty(username)
				&& !StringUtils.isNullOrEmpty(password)) {
			//UsernameはDBでチェックする
			boolean usercheck = loginAccess.userCheck(username);
			if (usercheck) {
				String pass = loginAccess.selectUserName(username).getPass();
				if (password.equals(pass)) {
					Login email = loginAccess.selectUserName(username);
					session.setAttribute("user", username);
				}else {
					out.print(gson.toJson(null));
					out.flush();
					out.close();
				}
			}else {
				out.print(gson.toJson(null));
				out.flush();
				out.close();
			}
		} else {
			session.getAttribute("user");
			//session.invalidate();
			if (StringUtils.isNullOrEmpty(ajax)) {
				RequestDispatcher rd = null;
				rd = request.getRequestDispatcher("/index.jsp");
				request.setAttribute("user", "user1111");
				rd.forward(request, response);
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
