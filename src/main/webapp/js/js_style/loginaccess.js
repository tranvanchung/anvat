$(document).ready(function(){

	// Get the modal
	var modal = document.getElementById('id01');

	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
	    if (event.target == modal) {
	        modal.style.display = "none";
	    }
	}

	$('#loginAccess').click(function() {
		login_access();
	  //  modal.style.display = "none";
	});

	$("#id01").on("keypress", function(event) {
		if (event.keyCode == 13) {
			event.preventDefault();
			login_access();
	       // modal.style.display = "none";
		}
	});

	// ユーザ登録ボタン
	$('#createuser').click(function() {
		createAccount();
	});
});



//ログインのボタンをチェックする
function login_access() {
	$('#notification').html("");
	var user = $("#username").val();
	var password = $("#password").val();
	$.ajax({
		type : 'POST',
		url : 'login',
		data : {
			"username" : user,
			"ajax" : "ok",
			"password" : password
		},
		success : function(data) {
//			var jSon = $.parseJSON(data);
//			console.log(data);
			if (!data) {
				location.reload(true)
				$('#loginForm').html("");
			} else {
				$('#notification').append("Tài khoản hoặc mật khẩu của bạn không tộn tại. Xin hãy kiểm tra lại");
			}
		},
		error : function() {
			$(location).attr('href','error404.html');
		}
	});

}

// ログアウトのボタンをチェック
function createAccount() {
	var userAccount = $("#userAccount").val();
	var passAccount = $("#passwordAccount").val();
	$.ajax({
		type : 'POST',
		url : 'createAccountController',
		data : {
			userAccount : userAccount,
			passAccount : passAccount
		},
		success : function(result) {
			var jSon = $.parseJSON(result);
			if (jSon == null) {
				// $('#createAccount').html("");
				$('#userName').append("Tài khoản đã tồn tại! Hãy dùng tài khoản khác");
			} else {
				$('#createAccount').html("");
				$('#userName').append(userAccount + "を登録しました");
				$('#login').show();
			}

		},
		error : function() {
			$('#createAccount').html("");
			$('#userName').html("");
			$('#userName').append(
					"<p style='color:yellow'>" + user + "を存在しません！</p>");
		}
	});
};
